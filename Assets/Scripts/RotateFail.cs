﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateFail : MonoBehaviour {
    // Configuration
    [Header("Configuration")]
    [Tooltip("Configuration")]
    public float degreesPerSec;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 0, this.degreesPerSec * Time.deltaTime);
	}
}
